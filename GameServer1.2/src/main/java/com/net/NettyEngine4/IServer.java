/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.net.NettyEngine4;

/**
 * @author : 陈磊 <br/>
 *         Date: 13-3-11<br/>
 *         Time: 下午4:06<br/>
 *         connectMethod:13638363871@163.com<br/>
 *         netty server  interface
 */
public interface IServer {

    /**
     * 初始化server配置
     * @return  boolean
     */
    boolean IntiServer();
    /**
     * run netty server
     */
    void run()throws Exception;

    /**
     * run epoll
     * @throws Exception
     */
    public void runNativeEpoll() throws Exception;
}
