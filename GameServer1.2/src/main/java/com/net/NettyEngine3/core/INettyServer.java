/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.net.NettyEngine3.core;

import org.jboss.netty.bootstrap.Bootstrap;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.ChannelPipelineFactory;

/**
 * Created with IntelliJ IDEA.
 * User: CHENLEI
 * Date: 12-10-30
 * Time: 上午11:03
 * To change this template use File | Settings | File Templates.
 * Netty core config
 */

public interface INettyServer {

    /**
     * 初始化server配置
     * @return  boolean
     */
    boolean IntiServer();
    /**
     * 启动底层通信
     * @throws Exception
     */
    void startServer() throws Exception;
	/**
	 * If thread pools or TCP/IP parameters or the pipeline factory need to be
	 * modified then it is this method that needs to be overriden.
	 * 
	 * @param optionsList
	 *            Used to set tcp ip options like noDelay etc.
	 */
	public void configureServerBootStrap(ServerBootstrap bootstrap, String[] optionsList)throws Exception;

	/**
	 * createServerBootstrap will create a pipeline factory and save it as a
	 * class variable. This method can then be used to retrieve that value.
	 * 
	 * @return Returns the channel pipeline factory that is associated with this
	 *         netty server.
	 */
	public ChannelPipelineFactory getPipelineFactory()throws Exception;

	/**
	 * @return Returns the created server bootstrap object.
	 */
	public Bootstrap getServerBootstrap()throws Exception;
    /**
     * 资源释放
     */
    void releaseServerResources()throws Exception;
}
