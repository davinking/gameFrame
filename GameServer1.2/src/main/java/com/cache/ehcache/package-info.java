/*
 * Copyright (c) 2015.
 * 游戏服务器核心代码编写人石头哥哥拥有使用权
 * 最终使用解释权归创心科技所有
 * 联系方式：E-mail:13638363871@163.com ;
 * 个人博客主页：http://my.oschina.net/chenleijava
 * powered by 石头哥哥
 */

/**

 @author 石头哥哥
 </P>
 Date:   2015/5/28
 </P>
 Time:   10:48
 </P>
 Package: dcServer-parent
 </P>

 注解：   基于ehcache  做进程内缓存 ，用于缓解数据库数据读取造成的压力

 缓存数据 和session中的数据一致


 */
package com.cache.ehcache;