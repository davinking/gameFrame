/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.cache.Memcached;

import net.rubyeye.xmemcached.exception.MemcachedException;

import java.util.concurrent.TimeoutException;

/**
 * @author 石头哥哥
 *         Date: 13-11-11</br>
 *         Time: 下午3:14</br>
 *         Package: Server.ExtComponents.memcache</br>
 *         注解：
 */
public interface   ImemCache {


    /**
     * set     key-----value
     * @param key
     * @param value
     * @return
     * @throws TimeoutException
     * @throws InterruptedException
     * @throws MemcachedException
     */
    public boolean set(final String key, final Object value)
            throws TimeoutException, InterruptedException, MemcachedException;

    /**
     *
     * @param key
     * @param exp 单位 秒
     * @param value
     * @return
     * @throws TimeoutException
     * @throws InterruptedException
     * @throws MemcachedException
     */
    public boolean set(final String key, final int exp, final Object value)
            throws TimeoutException, InterruptedException, MemcachedException;


    public boolean update(final String key, Object value) throws InterruptedException, MemcachedException, TimeoutException ;

    /**
     * @param key
     * @param value
     * @return
     * @throws TimeoutException
     * @throws InterruptedException
     * @throws MemcachedException
     */
    public boolean replace(final String key, final Object value)
            throws TimeoutException, InterruptedException, MemcachedException;

    /**
     * @param key
     * @param exp
     * @param value
     * @return
     * @throws TimeoutException
     * @throws InterruptedException
     * @throws MemcachedException
     */
    public boolean replace(final String key, final int exp, final Object value)
            throws TimeoutException, InterruptedException, MemcachedException;


    /**
     *
     * @param key
     * @param <T>
     * @return
     * @throws TimeoutException
     * @throws InterruptedException
     * @throws MemcachedException
     */
    public <T> T get(final String key) throws TimeoutException,
            InterruptedException, MemcachedException;




    /**
     * Delete key's date item from memcached
     *
     * @param key
     *            Operation timeout
     * @return
     * @throws TimeoutException
     * @throws InterruptedException
     * @throws MemcachedException
     * @since 1.3.2
     */
    public boolean delete(final String key)
            throws TimeoutException, InterruptedException, MemcachedException;

    /**
     * Delete key's date item from memcached
     *
     * @param key
     * @param opTimeout
     *            Operation timeout
     * @return
     * @throws TimeoutException
     * @throws InterruptedException
     * @throws MemcachedException
     * @since 1.3.2
     */
    public boolean delete(final String key, long opTimeout)
            throws TimeoutException, InterruptedException, MemcachedException;








}
