/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit.ip;

public interface Message {
    String bad_ip_file = "IP地址库文件错误";
    String unknown_country = "未知国家";
    String unknown_area = "未知地区";
}