/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.SpringRabbitmq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 石头哥哥</br>
 *         Date:14-3-8</br>
 *         Time:下午8:55</br>
 *         Package:com.dc.gameserver.extComponents.springRabbitmq</br>
 *         Comment：
 */
@Service
public class MyMqGatway {
    @Autowired
    private AmqpTemplate amqpTemplate;

    public void sendDataToCrQueue(Object obj) {
        //枚举 会更好点 ， ruting_key
        amqpTemplate.convertAndSend("queue_one_key", obj);
    }
}
