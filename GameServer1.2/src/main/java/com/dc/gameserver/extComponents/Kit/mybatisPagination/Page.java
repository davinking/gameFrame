/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit.mybatisPagination;

/**
 * @author 石头哥哥
 *         Date: 13-12-3</br>
 *         Time: 下午1:07</br>
 *         Package: com.dc.gameserver.extComponents.utilsKit</br>
 *         注解：分页工具类
 */
public class Page {

    // 分页查询下开始记录位置
    private int begin; //分页的第一个参数，从第几条数据开始（0为第一条）
    // 分页查询下结束位置
    private int end;
    // 每页显示记录数
    private int length=5; //每页显示数据
    // 查询结果总记录数
    private int count;
    // 当前页码
    private int current;
    // 总共页数
    private int total;

    public Page() {}
    /**
     * 构造函数
     *
     * @param begin     分页查询下开始记录位置
     * @param length    每页显示记录数
     */
    public Page(int begin, int length) {
        this.begin = begin;
        this.length = length;
        this.end = this.begin + this.length;
        this.current = (int) Math.floor((this.begin * 1.0d) / this.length) + 1;
    }

    /**
     * @param begin     分页查询下开始记录位置
     * @param length     每页显示记录数
     * @param count    当前页码
     */
    public Page(int begin, int length, int count) {
        this(begin, length);
        this.count = count;
    }

    /**
     * @return the begin
     */
    public int getBegin() {
        return begin;
    }

    /**
     * @return the end
     */
    public int getEnd() {
        return end;
    }

    /**
     * @param end
     *            the end to set
     */
    public void setEnd(int end) {
        this.end = end;
    }

    /**
     * @param begin
     *            the begin to set
     */
    public void setBegin(int begin) {
        this.begin = begin;
        if (this.length != 0) {
            this.current = (int) Math.floor((this.begin * 1.0d) / this.length) + 1;
        }
    }

    /**
     * 每页显示记录数
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * 每页显示记录数
     * @param length
     *            the length to set
     */
    public void setLength(int length) {
        this.length = length;
        if (this.begin != 0) {
            this.current = (int) Math.floor((this.begin * 1.0d) / this.length) + 1;
        }
    }

    /**
     * 查询结果总记录数
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * 查询结果总记录数
     * @param count
     *            the count to set
     */
    public void setCount(int count) {
        this.count = count;
        this.total = (int) Math.floor((this.count * 1.0d) / this.length);
        if (this.count % this.length != 0) {
            this.total++;
        }
    }

    /**
     * 当前页码
     * @return the current
     */
    public int getCurrent() {
        return current;
    }

    /**
     *
     * 当前页码
     * @param current
     *            the current to set
     */
    public void setCurrent(int current) {
        this.current = current;
    }

    /**
     * 总共页数
     * @return the total
     */
    public int getTotal() {
        if (total == 0) {
            return 1;
        }
        return total;
    }

    /**
     * 总共页数
     * @param total
     *            the total to set
     */
    public void setTotal(int total) {
        this.total = total;
    }
}
