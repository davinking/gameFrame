package com.dc.gameserver.extComponents.Kit.Orm;//package com.dc.gameserver.extComponents.utilsKit.Orm;
//
//import Config;
//import com.alibaba.druid.pool.DruidDataSource;
//import com.jolbox.bonecp.BoneCPConfig;
//import com.jolbox.bonecp.BoneCPDataSource;
//import org.apache.ibatis.mapping.Environment;
//import org.apache.ibatis.session.*;
//import org.apache.ibatis.transaction.TransactionFactory;
//import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
//import org.apache.log4j.xml.DOMConfigurator;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.sql.DataSource;
//import java.io.FileInputStream;
//import java.io.InputStream;
//import java.util.Properties;
//
///**
// * @author : 陈磊 <br/>
// *         Date: 13-2-26<br/>
// *         Time: 下午11:22<br/>
// *         connectMethod:13638363871@163.com<br/>
// *
// * 关于mybatis几个操作引用的生命周期
// *          1.SqlSessionFactoryBuilder ：  最佳范围是方法范围 (也就是本地方法变量)
// *          2.SqlSessionFactory ：一旦被创建,SqlSessionFactory 应该在你的应用执行期间都存在。没有理由来处理或重 新创建它。
// *          使用 SqlSessionFactory 的最佳实践是在应用运行期间不要重复创建多次(独立应用程序，创建单例来提供程序共享)
// *          Repository数据层
// *          3.SqlSession： 每个线程都应该有它自己的 SqlSession 实例。SqlSession 的实例不能被共享,也是线程 不安全的。因此最佳的范围是请求或方法范围。
// *          绝对不能将 SqlSession 实例的引用放在一个 类的静态字段甚至是实例字段中，因为那样是不安全的。
// *
// *          4.Mapper映射器，和sqlsession一样，生命周期应该是方法范围内
// *    说明：
// *        1、获取SqlSession，OrmProviderFactory.getSqlSession()
// *        2. 在方法范围内(为避免出错，请在finally中关闭)，用完后请关闭sqlsession，OrmProviderFactory.closeSqlSession(sqlsession);
// *           当然你也可以直接sqlsession.close(),但一定要关闭，否则连接数据源会被耗尽！！！
// *        3. 操作 SqlSession，数据访问层，请按照格式：
// *
////*                  //        SqlSession session = sqlSessionFactory.openSession();
////                    //            try {
////                    //            // do work
////                    //            } finally {
////                    //            session.close();
////                    //            }
// *          注意关闭session！！！
// *          OrmProviderFactory 可以提供两种方式bean：1.单例模式；2.spring管理bean
// *          在本服务应用中采用1的方式来处理
// */
//@SuppressWarnings("unchecked")
//public class OrmProviderFactory {
//    private static final Logger LOGGER = LoggerFactory.getLogger(OrmProviderFactory.class);
//    private static SqlSessionFactory SQL_SESSION_FACTORY=null;
//
//
//    public static final ThreadLocal MAP = new ThreadLocal();
//
//    private OrmProviderFactory(){IntiOrm();}
//    /**
//     * 初始化 mybatis
//     * @return void
//     * 基于非xml的方式配置mybatis，XML 映射仍然在大多数高级映射 (比如: 嵌套 Join 映射) 时需要。
//     * 出于这样的原因,如果存在 XML 配置文件的话,MyBatis 将会自动查找和加载一 个对等的 XML 文件(这种情况下,基于类路径下的 GameAccountMapper.class 类的类名,
//     * 那么 GameAccountMapper.xml 将会被加载)
//     */
//    private void IntiOrm() {
//        try {
//            TransactionFactory transactionFactory = new JdbcTransactionFactory();
//            Environment environment = new Environment("development", transactionFactory, getDataSource());
//            Configuration configuration = new Configuration(environment);
//            /**映射文件,注意mapper和xml配置文件的路径*/
//            configuration.addMappers(Config.DEFAULT_VALUE.FILE_PATH.MODULE_MAPPER_CONFIG);
//            SqlSessionFactoryBuilder sessionFactoryBuilder=new SqlSessionFactoryBuilder();
//            SQL_SESSION_FACTORY=sessionFactoryBuilder.build(configuration);
//        } catch (Exception e) {
//            LOGGER.error("初始化mybatis错误，请检查." +
//                    "... ...",e);
//        }
//    }
//    /**
//     *
//     * 获取数据源 游戏系统提供 BoneCp ，druid等数据源，具体采用方案，可以通过
//     * res/dataSourece.properties配置文件进行配置
//     * 优先选择druid数据源
//     * @return   DataSource
//     * @throws Exception
//     */
//    private  DataSource getDataSource() throws Exception {
//        InputStream fileInputStream= new FileInputStream(Config.DEFAULT_VALUE.FILE_PATH.DATASOURECE_CONFIG);
//        Properties properties  = new Properties();
//        properties.load(fileInputStream);
//        String poolName=properties.getProperty("name");
//        if (poolName.equalsIgnoreCase("bonecp")){ //bonecp
//            DataSource dataSource=new BoneCPDataSource(new BoneCPConfig(properties));
//            fileInputStream.close();
//            properties.clear();
//           return  dataSource;
//        }else if (poolName.equalsIgnoreCase("druid")){  //druid
//            DruidDataSource dataSource= new DruidDataSource();
//            dataSource.setUsername(properties.getProperty("username"));
//            dataSource.setName(poolName);
//            dataSource.setPassword(properties.getProperty("password"));
//            dataSource.setUrl(properties.getProperty("jdbcurl"));
//            dataSource.setDriverClassName(properties.getProperty("driverClass"));
//            dataSource.setInitialSize(Integer.parseInt(properties.getProperty("initialSize")));
//            dataSource.setMinIdle(Integer.parseInt(properties.getProperty("minIdle")));
//            dataSource.setMaxActive(Integer.parseInt(properties.getProperty("maxActive")));
//            dataSource.setMaxWait(Long.parseLong(properties.getProperty("maxWait")));
//
//            dataSource.setTimeBetweenEvictionRunsMillis(Long.parseLong(properties.getProperty("timeBetweenEvictionRunsMillis")));
//            dataSource.setMinEvictableIdleTimeMillis(Long.parseLong(properties.getProperty("minEvictableIdleTimeMillis")));
//            dataSource.setTestWhileIdle(Boolean.parseBoolean(properties.getProperty("testWhileIdle")));
//            dataSource.setTestOnReturn(Boolean.parseBoolean(properties.getProperty("testOnReturn")));
//            dataSource.setTestOnBorrow(Boolean.parseBoolean(properties.getProperty("testOnBorrow")));
//
//            dataSource.setValidationQuery(properties.getProperty("validationQuery"));
//            /**如果是mysql数据库,那么不需要配置下面两个参数*/
//            dataSource.setPoolPreparedStatements(Boolean.parseBoolean(properties.getProperty("poolPreparedStatements")));
//            dataSource.setMaxPoolPreparedStatementPerConnectionSize(Integer.parseInt(properties.getProperty("maxPoolPreparedStatementPerConnectionSize")));
//            dataSource.setUseUnfairLock(Boolean.parseBoolean(properties.getProperty("useUnfairLock")));
//            fileInputStream.close();
//            properties.clear();
//            return  dataSource;
//        }else {
//            return null;
//        }
//    }
//    /**
//     * 获取 SIMPLE sqlSession
//     * @return     sqlSession  自动提交事务 autoCommit true
//     */
//    public  SqlSession getSqlSession(){
//        return  getSqlSession(ExecutorType.SIMPLE);
//    }
//
//    /**
//     * 获取  批量执行更新语句 sqlSession
//     * @return  自动提交事务 autoCommit true
//     */
//    public  SqlSession getBeatchSqlSession(){
//      return getSqlSession(ExecutorType.BATCH);
//    }
//
//    /**
//     *获取  sqlSession
//     * @param executorType  SIMPLE, REUSE, BATCH   sqlSession  自动提交事务 autoCommit true
//     * @return
//     */
//    public  SqlSession getSqlSession(ExecutorType executorType){
//        SqlSession session = (SqlSession)MAP.get();
//        if (session==null){
//            session=SQL_SESSION_FACTORY.openSession(executorType,true);
//            MAP.set(session);
//        }
//        return  session;
//    }
//
//
//    /****
//     *   close session
//     */
//    public static void  closeSession(){
//        SqlSession session = (SqlSession)MAP.get();
//        MAP.set(null);
//        if (session!=null){
//            session.close();
//        }
//    }
//
//    /**
//     *  获取  OrmProviderFactory单例对象
//     * @return
//     */
//    public static OrmProviderFactory getInstance(){
//        return  singleton.providerFactory;
//    }
//
//    /**
//     *  OrmProviderFactory 内部类
//     */
//    private static final class singleton{
//        private static final OrmProviderFactory providerFactory=new OrmProviderFactory();
//    }
//
//    public static void main(String[]args){
//        //close the warn in logger,set system property
//        System.setProperty( "org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog" );
//        DOMConfigurator.configure(Config.DEFAULT_VALUE.FILE_PATH.LOG4J);
//        System.out.println( "---->"+getInstance().getSqlSession().getConnection());
//    }
//}
