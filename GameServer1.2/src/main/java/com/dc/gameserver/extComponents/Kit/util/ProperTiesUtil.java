/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * @auther:陈磊 <br/>
 * Date: 12-11-30<br/>
 * Time: 下午1:38<br/>
 * connectMethod:13638363871@163.com<br/>
 * 解析.properties文件工具类
 */
public class ProperTiesUtil {

    public static Properties getProPertis(String filePath){
        InputStream fileInputStream= null;
        try {
            fileInputStream = new FileInputStream(new File(filePath));
            Properties serverSettings    = new Properties();
            serverSettings.load(fileInputStream);
            fileInputStream.close();
            return serverSettings;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
    }

}
