/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.ServerCore.Service.character;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * 游戏对象   </br>
 * player对象不保存数据,所有数据均放在缓存中... ... </br>
 * PK状态中的玩家在预出牌区额牌同步之后，</br>
 * 后端开始计算出牌的结果并将结果返回给客户端   </br>
 */
@Service("player")
@Scope("prototype")
@SuppressWarnings("unchecked")
public class PlayerInstance extends GameCharacter {

}
