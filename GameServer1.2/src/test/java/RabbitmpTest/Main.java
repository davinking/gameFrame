/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package RabbitmpTest;

import java.util.HashMap;

/**
 * @author 石头哥哥 </br>
 *         gameFrame </br>
 *         Date:14-3-12 </br>
 *         Time:下午4:17 </br>
 *         Package:{@link RabbitmpTest}</br>
 *         Comment：      先运行一个消费者线程，然后开始产生大量的消息，这些消息会被消费者取走。
 *
 *         http://www.cnblogs.com/wangqing985/archive/2011/07/26/2117518.html
 */
public class Main {

    public Main() throws Exception{

        QueueConsumer consumer = new QueueConsumer("queue");
        Thread consumerThread = new Thread(consumer);
        consumerThread.start();

        Producer producer = new Producer("queue");

        for (int i = 0; i < 1000; i++) {
            HashMap<String,Integer> message = new HashMap<String,Integer>();
            message.put("message number", i);
            producer.sendMessage(message);
            System.out.println("Message Number "+ i +" sent.");
        }
    }

    /**
     * @param args
     * @throws SQLException
     * @throws IOException
     */
    public static void main(String[] args) throws Exception{
        new Main();
    }

}
