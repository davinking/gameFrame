/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author : 石头哥哥
 *         Project : LandlordsServer
 *         Date: 13-7-26
 *         Time: 下午1:21
 *         Connect: 13638363871@163.com
 *         packageName: PACKAGE_NAME
 */
public class testData {

    // 一天的毫秒数 60*60*1000*24
    private final static long DAY_MILLIS = 86400000;

    // 一小时的毫秒数 60*60*1000
    private final static long HOUR_MILLIS = 3600000;

    // 一分钟的毫秒数 60*1000
    private final static long MINUTE_MILLIS = 60000;

    //日期格式化串
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) throws Exception{
//        // 开始时间
//        Date startdate = DATE_FORMAT.parse("2010-01-01 08:01:00");
//        // 结束时间
//        Date enddate = DATE_FORMAT.parse("2010-09-03 00:00:00");
//
//        showTimedif(startdate, enddate);
//        Calendar c = Calendar.getInstance();
//        DateFormatUtils.format(c,"yyyy-MM-dd HH:mm:ss");
//        System.out.println( DateFormatUtils.format(c,"yyyy-MM-dd HH:mm:ss"));
//        System.out.println(DateUtils.addDay(1));
//        System.out.println(DateUtils.Expired(DateUtils.addDay(2),DateUtils.addDay(1)));
        Date date=new Date();
        System.out.println(date);
        System.out.println(date.getTime());
        Timestamp t = new Timestamp(date.getTime());
         System.out.println(t.getTime());
        System.out.println(t);
    }

    public static void showTimedif(Date start, Date end) {
        show(start.getTime(),end.getTime());
    }

    /**
     * 计算两个日期相差的天、小时、分钟
     *
     * @param start
     * @param end
     */
    public static void show(long start, long end) {
        long temp = end - start;
        System.out.print(DATE_FORMAT.format(new Date(start)) + " 与 " + DATE_FORMAT.format(new Date(end)));
        System.out.println("相差" + temp / DAY_MILLIS + "天" +
                (temp % DAY_MILLIS)/ HOUR_MILLIS + "小时" +
                ((temp % DAY_MILLIS) % HOUR_MILLIS)/ MINUTE_MILLIS + "分"+
                ((temp % DAY_MILLIS) % HOUR_MILLIS)% MINUTE_MILLIS/1000+"秒");
    }
}
