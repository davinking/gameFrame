package testJson;//package testJson;
//
//import com.alibaba.fastjson.JSON;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created with IntelliJ IDEA.
// * User: CHENLEI
// * Date: 12-11-17
// * Time: 下午12:34
// * To change this template use File | Settings | File Templates.
// * fastjson
// * JavaBean、Collection、Map、Date、Enum、泛型
// */
//public class testjson {
//
//
//    public static void main(String[] args) {
//        Map<String, Object> h = new HashMap<String, Object>();
//        h.put("1", "chenlei");
//        h.put("2", "oschina");
//
//
//        List<String> list = new ArrayList<String>();
//        list.add("qw");
//        list.add("qwsadsa");
//
//
//        Group group = new Group();
//        group.setId(0L);
//        group.setName("admin");
//
//        User guestUser = new User();
//        guestUser.setId(2L);
//        guestUser.setName("guest");
//
//        User rootUser = new User();
//        rootUser.setId(3L);
//        rootUser.setName("root");
//
//        group.getUsers().add(guestUser);
//        group.getUsers().add(rootUser);
//
////        javabean转化为json串
//        String jsonString = JSON.toJSONString(group);
//
//        System.out.println(jsonString);
//        String MapjsonString = JSON.toJSONString(h);
//
//        System.out.println(MapjsonString);
//
//        //collecction
//        String ListjsonString = JSON.toJSONString(list);
//
//        System.out.println(ListjsonString);
//
//        //json串
//        String str = "{\"id\":12,\"name\":\"chenlei\"}";
//
////        json串转化为javabean
//        Group group2 = JSON.parseObject(str, Group.class);
//
//        System.out.println(group2.getId());
//        System.out.println(group2.getName());
////        System.out.println(group2.getUsers());
////        System.out.println(group2.getUsers().get(0));
////        System.out.println(group2.getUsers().get(1));
//    }
//}
