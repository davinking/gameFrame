package nettyClient4;//package nettyClient4;
//
//import io.netty.buffer.ByteBuf;
//import io.netty.channel.ChannelHandlerContext;
//import io.netty.channel.ChannelInboundHandlerAdapter;
//import com.dc.gameserver.proto.gameserverprotoVo.playerIdResponse;
//
///**
// * @author : 石头哥哥
// *         Project : LandlordsServer
// *         Date: 13-8-7
// *         Time: 上午9:52
// *         Connect: 13638363871@163.com
// *         packageName: nettyClient4
// */
//public class clientHandler  extends ChannelInboundHandlerAdapter {
//
//    @Override
//    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//        ByteBuf byteBuf= (ByteBuf)msg;
//        int ID=byteBuf.readInt();
//        final int length = byteBuf.readableBytes();
//        byte[] array=new byte[length];
//        byteBuf.getBytes(byteBuf.readerIndex(), array, 0, length);
//        playerIdResponse.Builder playerIdResponse= com.dc.gameserver.proto.gameserverprotoVo.playerIdResponse.getDefaultInstance().newBuilderForType().mergeFrom(array, 0, length);
//        byteBuf.release();
//        byteBuf=null;
//        playerIdResponse=null;
//    }
//    @Override
//    public void channelActive(ChannelHandlerContext ctx) throws Exception {
//
//    }
//
//
//}
