/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.junit.Test;

/**
 * @author 石头哥哥 </br>
 *         gameFrame </br>
 *         Date:14-2-10 </br>
 *         Time:上午10:00 </br>
 *         Package:{@link PACKAGE_NAME}</br>
 *         Comment：
 */
public class smackTest {

    @Test
    public void smackT() throws XMPPException {
        // Create a connection to the jabber.org server.
        XMPPConnection conn1 = new XMPPConnection("jabber.org");
        conn1.connect();
       // Create a connection to the jabber.org server on a specific port.
        ConnectionConfiguration config = new ConnectionConfiguration("jabber.org", 5222);
        XMPPConnection conn2 = new XMPPConnection(config);
        conn2.connect();
    }

}
